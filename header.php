<?php
$serverUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/';
if (strpos($_SERVER['REQUEST_URI'], 'lserdarusic') !== false) {
    $serverUrl .= '~lserdarusic/';
}
include($_SERVER['DOCUMENT_ROOT'] . '/config.php');
session_start();
if (isset($_SESSION['login_user'])) {
    $user_check = $_SESSION['login_user'];
    $ses_sql = mysqli_query($db, "select username from registration where username = '$user_check' ");
    $row = mysqli_fetch_array($ses_sql, MYSQLI_ASSOC);
    $login_session = $row['username'];
}
?>

<head>
    <link rel="stylesheet" href="<?php echo $serverUrl ?>CSS/header.css">
    <link rel="stylesheet" href="<?php echo $serverUrl ?>CSS/footer.css">
</head>

<div class="wrapper">
    <header class="header">
        <div>
            <a href="<?php echo $serverUrl ?>">
                <h1>Arsenal FP</h1>
            </a>
        </div>
    </header>

    <nav class="nav">
        <a href="<?php echo $serverUrl ?>Pages/history.php">History</a>
        <a href="<?php echo $serverUrl ?>Pages/squad.php">Squad</a>
        <a href="<?php echo $serverUrl ?>Pages/gallery.php">Gallery</a>
        <a href="<?php echo $serverUrl ?>Pages/leaguetable.php">League table</a>
        <?php

        if (!isset($_SESSION['login_user'])) { ?>
        <a href="<?php echo $serverUrl ?>Pages/login.php">Login</a>
        <?php 
    } else { ?>
        <?php echo $_SESSION['login_user'] ?>
        <a href="<?php echo $serverUrl ?>Pages/logout.php">Logout</a>
        <?php 
    } ?>
    </nav>
</div>